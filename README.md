# AmadorUAVs Sim Infrastructure #
This repository contains all the source code for running the AmadorUAVs simulation infrastructure.
It includes:
- a docker-compose stack for quick and dirty local testing
- a VM stack, hosted on matthew-cloud.com, which more closely simulates a real world situation with multiple computers
- an obsoleted, broken kubernetes stack that doesn't work and is only around in case it's useful in the future

All the files for these stacks are located in their respective folders.

See the [wiki](https://gitlab.com/amadoruavs/sim/wikis/home) for more information.
